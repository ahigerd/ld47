using UnityEngine;

class PlayerBehaviour : CarBehaviour {
  public static PlayerBehaviour instance = null;
  public static MusicLoop bgm = null;
  public AudioClip coinSound = null;
  public int startingMoney = 100;
  public int money = 0;
  public int pickups = 0;
  public int winnings = 0;
  public int damages = 0;
  public int purchases = 0;
  public int startingDebt = 0;
  public int debt = 0;
  public int payments = 0;
  private float baseVolume = 1f;
  private int upgradeEngine = 0;
  private int upgradeAccel = 0;

  public override void Awake() {
    instance = this;
    base.Awake();
    bgm = GetComponentInChildren<MusicLoop>();
    baseVolume = bgm.volume;
  }

  public override void OnGameStart() {
    startingDebt = 100000;
    debt = 100000;
  }

  public override void OnLevelStart() {
    bgm.volume = baseVolume;
    bgm.Play();
    maxSpeed = baseMaxSpeed * (1 + .05f * upgradeEngine);
    accelCurve = baseAccelCurve * (1 + .1f * upgradeAccel);
    gravityScale = 1 + .1f * upgradeEngine;
    readyForLap = -1;
    bestLapTime = Mathf.Infinity;
  }

  public override void OnPlayerSpawn() {
    laps = 0;
    engineNoise.volume = 0;
    engineNoise.Play();
    ResetPosition(TrackBehaviour.startPositions[startPosition]);
    baseEngineVolume = 0.6f;
    engineAttenuation = 0.12f;
    place = startPosition;
  }

  public override float GetAccelerator() {
    if (GameManager.state == GameState.LevelComplete) {
      return base.GetAccelerator();
    }
    return Input.GetAxis("Vertical");
  }

  public override float GetSteering() {
    if (GameManager.state == GameState.LevelComplete) {
      return base.GetSteering();
    }
    return Input.GetAxis("Horizontal");
  }

  public override float GetDraft() {
    if (laps < 1) {
      return 0;
    }
    RaycastHit hit;
    bool didHit = Physics.BoxCast(
      transform.position + transform.forward * 2.2f,
      new Vector3(6f, 32f, 0.1f),
      (transform.forward + transform.up).normalized,
      out hit,
      transform.rotation,
      50f,
      1 << 9);
    Vector3 result = Vector3.zero;
    if (didHit) {
      float mag = Mathf.Clamp01(16f / hit.distance);
      Vector3 draft = (hit.transform.position - transform.position).normalized * mag;
      float velMag = Vector3.Project(hit.rigidbody.velocity, transform.forward).magnitude * .02f;
      result += draft * velMag;
      result = Vector3.Project(result, transform.forward) * .3f;
      if (Vector3.Angle(result, transform.forward) != 0) {
        return 0;
      }
    }
    return Mathf.Clamp(result.magnitude, 0, .5f) * 2f;
  }

  public void OnGUI() {
    if (GameManager.state == GameState.LevelComplete && bgm.volume <= 0) {
      ShopGUI();
      return;
    } else if (GameManager.state != GameState.Playing) {
      return;
    }
    float absSpeed = rb.velocity.magnitude;
    float angle = Vector3.SignedAngle(transform.forward, rb.velocity, transform.up);
    float speed = absSpeed;
    if (Mathf.Abs(angle) > 1) {
      speed = absSpeed * Mathf.Cos(angle);
    }
    float posAngle = Mathf.Atan2(transform.position.y - TrackBehaviour.activeRadius, transform.position.z) * Mathf.Rad2Deg;
    int numLaps = TrackBehaviour.current.numLaps;
    string hudText = string.Format(
      "{0,3} mph\n{1,3} kph\nPlace {4} - Lap {2} / {3}",
      (int)(absSpeed * 2.237),
      (int)(absSpeed * 3.6),
      laps < numLaps ? laps : numLaps,
      numLaps,
      place);
    if (laps > 1) {
      string lastLapText = string.Format("{0:D2}:{1:D2}.{2:D3}", (int)(lastLapTime / 60), (int)(lastLapTime % 60), (int)((lastLapTime * 1000) % 1000));
      string bestLapText = string.Format("{0:D2}:{1:D2}.{2:D3}", (int)(bestLapTime / 60), (int)(bestLapTime % 60), (int)((bestLapTime * 1000) % 1000));
      hudText += string.Format("\nLast Lap: {0}\nBest Lap: {1}", lastLapText, bestLapText);
    }
    GUI.Label(new Rect(0, 0, 150, 90), hudText, GUI.skin.box);
    GUI.Label(new Rect(Screen.width - 150, 0, 150, 20), string.Format("Earned: {0}", pickups), GUI.skin.box);
  }

  public override void Update() {
    if (GameManager.state == GameState.LevelComplete) {
      bgm.volume -= Time.deltaTime * .1f;
    } else if (GameManager.state == GameState.Spawning && isGrounded) {
      GameManager.instance.StartLife();
    }
    base.Update();
    if (GameManager.state == GameState.Playing && laps > TrackBehaviour.current.numLaps) {
      switch (place) {
        case 1:
          winnings = 1000;
          break;
        case 2:
          winnings = 500;
          break;
        case 3:
          winnings = 200;
          break;
        default:
          winnings = 0;
          break;
      }
      if (place > 4) {
        // Don't advance to the next level if you did poorly
        GameManager.level -= 1;
      }
      money += winnings;
      float oldBest = PlayerPrefs.GetInt("score " + (GameManager.level - 1), -1) / 1000f;
      Utility.Log("{0} vs {1}", oldBest, bestLapTime);
      if (oldBest <= 0 || oldBest > bestLapTime) {
        GameManager.SaveHighScore(GameManager.level, (int)(bestLapTime * 1000));
      }
      GameManager.instance.CompleteLevel();
    }
  }

  public override void OnTriggerEnter(Collider other) {
    if (GameManager.state == GameState.Playing && other.gameObject.CompareTag("Pickup")) {
      money += 100;
      pickups += 100;
      Object.Destroy(other.gameObject);
      GameManager.PlaySound(coinSound, 0, 0.7f);
    } else {
      base.OnTriggerEnter(other);
    }
  }

  private void ShopGUI() {
    GUIStyle receiptStyle = Menu.GetBackgroundStyle(Color.white);

    GUIStyle headerStyle = new GUIStyle(GUI.skin.label);
    headerStyle.alignment = TextAnchor.MiddleCenter;
    headerStyle.normal.textColor = Color.black;
    headerStyle.fontStyle = FontStyle.Bold;

    GUIStyle leftStyle = new GUIStyle(GUI.skin.label);
    leftStyle.alignment = TextAnchor.MiddleLeft;
    leftStyle.normal.textColor = Color.black;

    GUIStyle rightStyle = new GUIStyle(leftStyle);
    rightStyle.alignment = TextAnchor.MiddleRight;

    GUIStyle redStyle = new GUIStyle(rightStyle);
    redStyle.normal.textColor = Color.red;

    int cx = Screen.width / 2;
    int cy = Screen.height / 2;
    GUI.Box(new Rect(cx - 200, cy - 200, 400, 400), "", GUI.skin.box);
    GUI.Box(new Rect(cx - 200, cy - 200, 400, 400), "", GUI.skin.box);

    GUI.Box(new Rect(cx - 190, cy - 190, 180, 330), "", receiptStyle);

    GUI.Label(new Rect(cx - 180, cy - 188, 160, 24), "Your Account", headerStyle);
    GUI.Label(new Rect(cx - 180, cy - 164, 160, 24), "Starting Funds", leftStyle);
    GUI.Label(new Rect(cx - 180, cy - 164, 160, 24), startingMoney.ToString(), startingMoney >= 0 ? rightStyle : redStyle);
    GUI.Label(new Rect(cx - 180, cy - 140, 160, 24), "Entry Fee", leftStyle);
    GUI.Label(new Rect(cx - 180, cy - 140, 160, 24), "-100", redStyle);
    GUI.Label(new Rect(cx - 180, cy - 116, 160, 24), "Repairs", leftStyle);
    GUI.Label(new Rect(cx - 180, cy - 116, 160, 24), (-damages).ToString(), damages > 0 ? redStyle : rightStyle);
    GUI.Label(new Rect(cx - 180, cy - 92, 160, 24), "Winnings", leftStyle);
    GUI.Label(new Rect(cx - 180, cy - 92, 160, 24), winnings.ToString(), rightStyle);
    GUI.Label(new Rect(cx - 180, cy - 68, 160, 24), "Collected", leftStyle);
    GUI.Label(new Rect(cx - 180, cy - 68, 160, 24), pickups.ToString(), rightStyle);
    GUI.Label(new Rect(cx - 180, cy - 44, 160, 24), "Purchases", leftStyle);
    GUI.Label(new Rect(cx - 180, cy - 44, 160, 24), (-purchases).ToString(), purchases > 0 ? redStyle : rightStyle);
    GUI.Label(new Rect(cx - 180, cy - 20, 160, 24), "Total Funds", leftStyle);
    GUI.Label(new Rect(cx - 180, cy - 20, 160, 24), money.ToString(), money >= 0 ? rightStyle : redStyle);

    if (payments > 0) {
      GUI.Label(new Rect(cx - 180, cy + 40, 160, 24), "Your Loan", headerStyle);
      GUI.Label(new Rect(cx - 180, cy + 64, 160, 24), "Previous Debt", leftStyle);
      GUI.Label(new Rect(cx - 180, cy + 64, 160, 24), startingDebt.ToString(), redStyle);
      GUI.Label(new Rect(cx - 180, cy + 88, 160, 24), "Payments", leftStyle);
      GUI.Label(new Rect(cx - 180, cy + 88, 160, 24), payments.ToString(), rightStyle);
    } else {
      GUI.Label(new Rect(cx - 180, cy + 88, 160, 24), "Your Loan", headerStyle);
    }
    GUI.Label(new Rect(cx - 180, cy + 112, 160, 24), "Debt Remaining", leftStyle);
    GUI.Label(new Rect(cx - 180, cy + 112, 160, 24), debt.ToString(), debt >= 0 ? redStyle : rightStyle);

    GUIStyle bigStyle = new GUIStyle(GUI.skin.label);
    bigStyle.fontSize = 24;
    bigStyle.alignment = TextAnchor.MiddleCenter;
    GUI.Label(new Rect(cx + 10, cy - 190, 180, 40), "Shop", bigStyle);

    int accelCost = (upgradeAccel + 1) * 1000;
    int engineCost = (upgradeEngine + 1) * 1000;

    GUI.enabled = accelCost <= money;
    if (GUI.Button(new Rect(cx + 10, cy - 135, 180, 80), string.Format("Acceleration\n\n(Cost: {0})", accelCost))) {
      upgradeAccel++;
      money -= accelCost;
      purchases += accelCost;
    }

    GUI.enabled = engineCost <= money;
    if (GUI.Button(new Rect(cx + 10, cy - 45, 180, 80), string.Format("Top Speed\n\n(Cost: {0})", engineCost))) {
      upgradeEngine++;
      money -= engineCost;
      purchases += engineCost;
    }

    GUI.enabled = 1000 <= money;
    if (GUI.Button(new Rect(cx + 10, cy + 45, 180, 80), "Loan Payment\n\n(Cost: 1000)")) {
      money -= 1000;
      payments += 1000;
      debt -= 1000;
      purchases += 1000;
    }

    GUI.enabled = true;
    if (GUI.Button(new Rect(cx - 190, cy + 150, 380, 40), "Race! (Entry Fee: 100)")) {
      startingMoney = money;
      money -= 100;
      damages = 0;
      winnings = 0;
      pickups = 0;
      purchases = 0;
      payments = 0;
      GameManager.instance.NewLevel();
    }
  }

  public override void OnRespawn() {
    damages += 250;
  }
}
