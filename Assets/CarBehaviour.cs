using UnityEngine;

class CarBehaviour : LevelAwareBehaviour {
  public static Collider lapTape = null;
  public static float lapTapePos = 0;
  protected TextMesh placeText = null;
  protected AudioSource engineNoise = null;
  protected float baseEngineVolume = .5f;
  protected float engineAttenuation = 0.06f;
  public GameObject wheelPrefab = null;
  public float maxSpeed = 40f;
  public float steerRange = 30f;
  public float accelCurve = 1f;
  public float brakeForce = 200f;
  public float dragFactor = 0.1f;
  protected float baseMaxSpeed, baseAccelCurve;
  public int startPosition = 0;
  private int _place = 0;
  public int laps = -1;
  public float distance = 0;
  public Vector3 recoil = Vector3.zero;
  protected float lapStartTime = 0;
  protected float lastLapTime = 0;
  protected float bestLapTime = Mathf.Infinity;
  protected int readyForLap = -1;
  protected GameObject[] wheels;
  protected Rigidbody rb;
  protected Vector3 gravity = Vector3.zero;
  protected float notOverGround = 0;
  protected bool isGrounded = false;
  protected Vector3 recoverPoint = Vector3.zero;
  protected RaycastHit following;
  protected bool isFollowing;
  protected float steerSmooth = 0f;
  protected float draftMag = 0f;
  protected float gravityScale = 1f;
  private Vector3 lastPos;
  private float prevTurn = 0;

  public int place {
    get {
      return _place;
    }
    set {
      _place = value;
      placeText.text = value.ToString();
    }
  }

  public virtual void Awake() {
    placeText = GetComponentInChildren<TextMesh>();
    engineNoise = GetComponent<AudioSource>();
    engineNoise.Stop();
    MeshFilter mf = wheelPrefab.GetComponentInChildren<MeshFilter>();
    mf.mesh = ShipBlock.prefabs.wheel;
    rb = GetComponent<Rigidbody>();
    rb.centerOfMass = Vector3.down * 10;
    wheels = new GameObject[4] {
      Object.Instantiate(wheelPrefab, transform.position + new Vector3(-1, .5f, +1), Quaternion.identity, transform),
      Object.Instantiate(wheelPrefab, transform.position + new Vector3(+1, .5f, +1), Quaternion.identity, transform),
      Object.Instantiate(wheelPrefab, transform.position + new Vector3(-1, .5f, -1), Quaternion.identity, transform),
      Object.Instantiate(wheelPrefab, transform.position + new Vector3(+1, .5f, -1), Quaternion.identity, transform),
    };
    baseMaxSpeed = maxSpeed;
    baseAccelCurve = accelCurve;
  }

  public override void OnPlayerSpawn() {
    laps = 0;
    bestLapTime = Mathf.Infinity;
    lastLapTime = 0;
    engineNoise.volume = 0;
    engineNoise.Play();
    ResetPosition(TrackBehaviour.startPositions[startPosition]);
    float factor = Random.value - .5f;
    float scaleFactor = .9f + (GameManager.level * .5f + factor) * .5f;
    maxSpeed = baseMaxSpeed * scaleFactor;
    scaleFactor = 1f + (GameManager.level - factor) * .5f;
    accelCurve = baseAccelCurve * scaleFactor;
    lastPos = transform.position;
    readyForLap = -1;
  }

  public void FixedUpdate() {
    if (!GameManager.IsRunning()) {
      return;
    }

    float calcSpeed = (transform.position - lastPos).magnitude / Time.deltaTime;
    lastPos = transform.position;

    float newTurn = GetSteering() * steerRange;
    if (Mathf.Abs(newTurn) < 1) {
      newTurn = 0;
    }
    float turn = Mathf.SmoothDamp(prevTurn, newTurn, ref steerSmooth, 0.1f);
    prevTurn = turn;
    Vector3 rot = new Vector3(0, turn, 0);
    wheels[0].transform.localEulerAngles = rot;
    wheels[1].transform.localEulerAngles = rot;

    RaycastHit hitInfo;
    bool overGround = Physics.Raycast(transform.position + transform.up, -transform.up, out hitInfo, 10f, 1 << 8);
    bool oldGround = isGrounded;
    isGrounded = overGround && (hitInfo.distance < 2f);
    if (overGround) {
      if (isGrounded) {
        recoverPoint = transform.position;
      }
      gravity = -hitInfo.normal * Physics.gravity.magnitude;
      notOverGround -= Time.deltaTime;
      if (notOverGround < 0) {
        notOverGround = 0;
      }
    } else {
      notOverGround += Time.deltaTime;
      if (notOverGround > 2f) {
        OnRespawn();
        Vector2 bounds = TrackBehaviour.current.BoundsAt(TrackBehaviour.current.SegmentAt(recoverPoint));
        ResetPosition(new Vector3((bounds.x + bounds.y) / 2f, recoverPoint.y, recoverPoint.z));
      }
    }
    rb.isKinematic = isGrounded;
    if (!isGrounded) {
      if (recoil.magnitude > 0) {
        rb.AddForce(recoil, ForceMode.Acceleration);
        if (recoil.magnitude < Time.deltaTime) {
          recoil = Vector3.zero;
        } else {
          recoil = Vector3.ClampMagnitude(recoil - recoil.normalized * Time.deltaTime * 4f, 2f);
        }
      }
      if (GameManager.state == GameState.Spawning) {
        rb.AddForce(gravity * .095f, ForceMode.Acceleration);
        engineNoise.volume += baseEngineVolume / 4f * Time.deltaTime;
      } else {
        rb.AddForce(gravity, ForceMode.Acceleration);
      }
      return;
    }

    float gas = GetAccelerator();
    if (gas < 0) {
      gas *= .25f;
    }

    float absSpeed = calcSpeed;
    float angle = Vector3.SignedAngle(transform.forward, rb.velocity, transform.up) * Mathf.Deg2Rad;
    float speed = absSpeed * Mathf.Cos(angle);
    float slip = absSpeed * Mathf.Sin(angle);
    float newDraftMag = GetDraft() * 2f;
    if (newDraftMag > draftMag) {
      draftMag = newDraftMag;
    } else if (draftMag > 0) {
      draftMag -= Time.deltaTime * .6f;
    } else {
      draftMag = 0;
    }
    if (absSpeed < 0.001) {
      speed = 0;
      slip = 0;
    }

    float targetSpeed = maxSpeed * gas * (1f + draftMag) * (1f + place * .02f);
    bool braking = ((targetSpeed > 0) != (speed >= 0));
    float deltaSpeed = (braking ? brakeForce : accelCurve) * targetSpeed / (1f + speed * speed / (1f + draftMag));
    deltaSpeed -= (speed) * (dragFactor * Time.deltaTime);
    float newSpeed = (speed + deltaSpeed * Time.deltaTime);
    if (braking && draftMag > 0) {
      draftMag *= 1f - 3f * Time.deltaTime;
    }
    if (targetSpeed > 0) {
      if (newSpeed > targetSpeed) {
        newSpeed = targetSpeed;
      }
    } else if (targetSpeed < 0) {
      if (newSpeed < targetSpeed) {
        newSpeed = targetSpeed;
      }
    } else if ((speed < 0) == (newSpeed > 0) || Mathf.Abs(newSpeed) < .1f) {
      newSpeed = 0;
    } else if (braking && Mathf.Abs(newSpeed) < 10f) {
      newSpeed /= 2;
    }
    Vector3 newPos = transform.position + transform.forward * newSpeed * Time.deltaTime;

    if (recoil.magnitude > 0) {
      newPos += recoil * Time.deltaTime;
      if (recoil.magnitude < Time.deltaTime) {
        recoil = Vector3.zero;
      } else {
        recoil = Vector3.ClampMagnitude(recoil - recoil.normalized * Time.deltaTime * 4f, 2f);
      }
    }

    Vector3 center = TrackBehaviour.center;
    center.x = newPos.x;
    Vector3 up = (center - newPos).normalized;
    Vector3 fwd = transform.forward + transform.right * Mathf.Sin(turn * Mathf.Deg2Rad * .1f) * newSpeed * Time.deltaTime;
    fwd = Vector3.ProjectOnPlane(fwd, up);
    Quaternion newRot = Quaternion.LookRotation(fwd, up);
    newPos = center - up * TrackBehaviour.activeRadius;

    rb.MovePosition(newPos);
    rb.MoveRotation(newRot);

    CheckCollisions();
  }

  public virtual void Update() {
    if (GameManager.state != GameState.Playing && GameManager.state != GameState.LevelComplete) {
      return;
    }

    float pitch = 1 + Mathf.Abs(GetAccelerator()) * 20;
    if (!isGrounded) {
      pitch *= 1 + notOverGround;
    }
    engineNoise.pitch = pitch;
    if (GameManager.state == GameState.LevelComplete) {
      engineNoise.volume -= Time.deltaTime * .05f;
    } else {
      engineNoise.volume = (baseEngineVolume - engineAttenuation * Mathf.Log(pitch)) * .75f;
    }

    float segment = TrackBehaviour.current.SegmentAt(transform.position);
    float angle = (TrackBehaviour.current.AngleOf(transform.position) - lapTapePos + 360f) % 360f;
    distance = laps * 360 + angle;
    if (segment > 90 && segment < 180) {
      if (readyForLap > 0) {
        readyForLap = 0;
      }
    } else if (segment < 270) {
      if (readyForLap == 0) {
        readyForLap = 1;
      }
    } else if (readyForLap == 1 && segment >= 270) {
      readyForLap = 2;
    }
  }

  public void ResetPosition(Vector3 pos) {
    recoil = Vector3.zero;
    steerSmooth = 0;
    prevTurn = 0;
    draftMag = 0;
    rb.velocity = Vector3.zero;
    rb.angularVelocity = Vector3.zero;
    rb.ResetInertiaTensor();
    transform.position = pos;
    transform.rotation = TrackBehaviour.tangent(pos);
    transform.position += TrackBehaviour.normalVector(pos) * 5;
    notOverGround = 0;
    isGrounded = false;
    lastPos = transform.position;
  }

  public virtual float GetAccelerator() {
    isFollowing = Physics.BoxCast(
      transform.position + transform.forward * 2.2f,
      new Vector3(4f, 8f, 0.1f),
      transform.forward,
      out following,
      transform.rotation * Quaternion.AngleAxis(30, transform.right),
      16f,
      1 << 9);
    if (isFollowing && Mathf.Abs(following.transform.position.x - transform.position.x) < 1.5f) {
      return Mathf.Clamp01(following.distance / 12f);
    }
    return 1f;
  }

  public virtual float GetSteering() {
    int segment = TrackBehaviour.current.SegmentAt(transform.position);
    Vector2 bounds = TrackBehaviour.current.BoundsAt((segment + 1) % 360);
    Vector2 prevBounds = TrackBehaviour.current.BoundsAt((segment + 0) % 360);
    float center = (bounds.x + bounds.y) * .5f;
    float oldCenter = (prevBounds.x + prevBounds.y) * .5f;

    Vector3 up = TrackBehaviour.normalVector(transform.position);
    Vector3 straight = TrackBehaviour.tangentVector(transform.position);
    straight.x = (center - oldCenter) * .45f;
    straight = straight.normalized;
    Vector3 forward = Vector3.ProjectOnPlane(transform.forward, up);

    float halfWidth = TrackBehaviour.current.WidthAt(segment) * .5f;
    bool shouldNormalize = false;
    if (isFollowing) {
      float tx = following.transform.position.x - center;
      if (tx < halfWidth * -.3f) {
        tx = (halfWidth + tx) / 2f;
      } else if (tx > halfWidth * .3f) {
        tx = (-halfWidth + tx) / 2f;
      } else if (transform.position.x < 0) {
        tx = (-halfWidth + tx) / 2f;
      } else {
        tx = (halfWidth + tx) / 2f;
      }
      straight.x = tx - transform.position.x;
      shouldNormalize = straight.x != 0;
    } else if (Mathf.Abs(transform.position.x - center) > halfWidth * .5f) {
      straight.x = Mathf.Clamp((Mathf.Abs(transform.position.x - center) - halfWidth * .5f) / (halfWidth * .5f) * -Mathf.Sign(transform.position.x - center), -1, 1);
    }
    if (shouldNormalize && straight.x != 0) {
      straight.x = Mathf.Sign(straight.x) * Mathf.Log(Mathf.Abs(straight.x));
    }

    float angle = Vector3.SignedAngle(forward, straight, up) * 1.5f;
    float result = Mathf.Clamp(angle, -steerRange, steerRange) / steerRange;
    return result;
  }

  public virtual float GetDraft() {
    return 0f;
  }

  public virtual void OnTriggerEnter(Collider other) {
    if (other == lapTape && (readyForLap < 0 || readyForLap == 2)) {
      if (laps > 0) {
        lastLapTime = Time.time - lapStartTime;
        if (lastLapTime < bestLapTime) {
          bestLapTime = lastLapTime;
        }
      }
      lapStartTime = Time.time;
      readyForLap = 0;
      laps += 1;
    }
  }

  public virtual void OnRespawn() {}

  void CheckCollisions() {
    BoxCollider me = GetComponent<BoxCollider>();
    Collider[] others = Physics.OverlapBox(transform.position, me.size, transform.rotation, 1 << 9, QueryTriggerInteraction.Collide);
    Vector3 dir;
    float dist;
    foreach (var other in others) {
      if (other.gameObject == gameObject) {
        continue;
      }
      if (!other.gameObject.CompareTag("AI") && !other.gameObject.CompareTag("Player")) {
        continue;
      }
      bool pen = Physics.ComputePenetration(
        me, transform.position, me.transform.rotation,
        other, other.transform.position, other.transform.rotation,
        out dir, out dist
      );
      if (!pen) {
        continue;
      }
      CarBehaviour otherCar = other.GetComponent<CarBehaviour>();
      otherCar.rb.MovePosition(otherCar.transform.position + dir * dist * -.5f);
      rb.MovePosition(transform.position + dir * dist * .5f);
      float vel = Mathf.Clamp((rb.velocity - otherCar.rb.velocity).magnitude * .3f, 0, 3f);
      otherCar.recoil += -dir * vel;
      recoil += dir * vel;
    }
  }
}
