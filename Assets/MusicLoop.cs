using UnityEngine;

class MusicLoop : MonoBehaviour {
  public AudioClip intro = null;
  public AudioClip loop = null;
  public bool playOnStart = true;
  [HideInInspector]
  public bool isParent = true;
  [HideInInspector]
  public AudioSource introSource = null;
  [HideInInspector]
  public AudioSource loopSource = null;

  public float volume {
    get {
      return introSource.volume;
    }

    set {
      introSource.volume = value;
      loopSource.volume = value;
    }
  }

  void Awake() {
    if (isParent && introSource == null) {
      isParent = false;
      introSource = GetComponent<AudioSource>();
      introSource.playOnAwake = false;

      loopSource = Object.Instantiate(introSource, transform).GetComponent<AudioSource>();

      introSource.loop = false;
      introSource.clip = intro;

      loopSource.loop = true;
      loopSource.clip = loop;
      isParent = true;
    }
  }

  void Start() {
    if (isParent && playOnStart) {
      Play();
    }
  }

  public void Play() {
    double startTime = AudioSettings.dspTime + 0.1;
    double loopStartTime = startTime + intro.length;
    introSource.PlayScheduled(startTime);
    loopSource.PlayScheduled(loopStartTime);
    loopSource.loop = true;
  }

  public void Stop() {
    introSource.Stop();
    loopSource.Stop();
  }
}
