using UnityEngine;

class CoinBehaviour : MonoBehaviour {
  void Awake() {
    GetComponent<MeshFilter>().mesh = ShipBlock.prefabs.wheel;
    transform.Rotate(new Vector3(0, 360 * Random.value, 0));
  }

  void Update() {
    transform.Rotate(new Vector3(0, 60 * Time.deltaTime, 0));
  }
}
