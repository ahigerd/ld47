using UnityEngine;
using System.Collections.Generic;

class TrackBehaviour : LevelAwareBehaviour {
  static public TrackBehaviour current = null;
  static public Vector3 center = Vector3.zero;
  static public float activeRadius = 0;
  static public Vector3[] startPositions = new Vector3[8];
  public GameObject lapTapePrefab = null;
  public GameObject blockPrefab = null;
  public GameObject npcPrefab = null;
  public GameObject coinPrefab = null;
  public List<CarBehaviour> cars = new List<CarBehaviour>();
  public float radius = 50;
  public float width = 20;
  public int numLaps = 5;
  private float repLen = 1;

  public override void OnLevelStart() {
    foreach (var pickup in GameObject.FindGameObjectsWithTag("Pickup")) {
      Object.Destroy(pickup);
    }

    foreach (var tape in GameObject.FindGameObjectsWithTag("LapTape")) {
      Object.Destroy(tape);
    }

    current = this;
    radius = activeRadius = 50 * (1 + GameManager.level) * .5f;
    width = 15 + 20 / GameManager.level;
    center = transform.position + radius * transform.up;
    MeshBuilder mb = new MeshBuilder();
    float halfWidth = width / 2;
    Vector3 lastLeft = new Vector3(-halfWidth, 0, 0);
    Vector3 lastRight = new Vector3(+halfWidth, 0, 0);
    repLen = 180 - radius;
    for (int i = 1; i < 361; i++) {
      AddSegment(mb, 0, halfWidth, i, ref lastLeft, ref lastRight, i == 360 ? "last" : "");
    }
    Mesh mesh = mb.GetMesh();
    GetComponent<MeshFilter>().mesh = mesh;
    GetComponent<MeshCollider>().sharedMesh = mesh;

    numLaps = 5;
    foreach (var car in cars) {
      if (car != PlayerBehaviour.instance) {
        Object.Destroy(car.gameObject);
      }
    }
    cars.Clear();
    for (int i = 0; i < 8; i++) {
      if (PlayerBehaviour.instance.startPosition == i) {
        cars.Add(PlayerBehaviour.instance);
        continue;
      }
      CarBehaviour car = Object.Instantiate(npcPrefab).GetComponent<CarBehaviour>();
      car.startPosition = i;
      car.place = i;
      cars.Add(car);
    }

    GameManager.instance.SpawnLife();
  }

  private void AddSegment(MeshBuilder mb, float midX, float halfWidth, int i, ref Vector3 lastLeft, ref Vector3 lastRight, string key) {
    float t = (i + 270) * Mathf.Deg2Rad;
    Vector3 left, right;
    Vector2 bounds = BoundsAt(i);
    if (i == 360) {
      left = new Vector3(bounds.x, 0, 0);
      right = new Vector3(bounds.y, 0, 0);
    } else {
      float x = Mathf.Cos(t) * radius;
      float y = Mathf.Sin(t) * radius + radius;
      left = new Vector3(bounds.x, y, x);
      right = new Vector3(bounds.y, y, x);
    }
    mb.Triangle(key, right, lastLeft, left, new Vector2(1, i / repLen), new Vector2(0, (i - 1) / repLen), new Vector2(0, i / repLen));
    mb.Triangle(key, lastRight, lastLeft, right, new Vector2(1, (i - 1) / repLen), new Vector2(0, (i - 1) / repLen), new Vector2(1, i / repLen));
    Vector3 center = transform.position + (left + right + lastLeft + lastRight) * .25f;
    if (i < 18 && i % 4 == 1) {
      int si = i / 4;
      switch (si) {
      case 0:
        startPositions[6] = center + Vector3.left * (halfWidth * .8f);
        startPositions[7] = center + Vector3.left * (halfWidth * .1f);
        break;
      case 1:
        startPositions[4] = center + Vector3.left * (halfWidth * .5f);
        startPositions[5] = center + Vector3.right * (halfWidth * .2f);
        break;
      case 2:
        startPositions[2] = center + Vector3.left * (halfWidth * .2f);
        startPositions[3] = center + Vector3.right * (halfWidth * .5f);
        break;
      case 3:
        startPositions[0] = center + Vector3.right * (halfWidth * .1f);
        startPositions[1] = center + Vector3.right * (halfWidth * .8f);
        break;
      default:
        GameObject tape = Object.Instantiate(
          lapTapePrefab,
          transform.position + (left + right) * .5f,
          tangent(t),
          transform
        );
        tape.transform.position += tape.transform.localScale.y * .5f * tape.transform.up;
        Vector3 scale = tape.transform.localScale;
        scale.x = width * 2;
        tape.transform.localScale = scale;
        CarBehaviour.lapTape = tape.GetComponent<Collider>();
        CarBehaviour.lapTapePos = AngleOf(tape.transform.position);
        break;
      }
    } else if (i > 20 && i % 15 == 0) {
      GameObject coin = Object.Instantiate(
        coinPrefab,
        center + new Vector3((Random.value - .5f) * width * .8f, 0, 0),
        tangent(t),
        transform
      );
      coin.transform.position += coin.transform.localScale.y * .5f * coin.transform.up;
    }
    lastLeft = left;
    lastRight = right;
  }

  public float WidthAt(int segment) {
    return width;
  }

  public Vector2 BoundsAt(int segment) {
    int waves = 1 + (GameManager.level - 1) / 2;
    if (waves < 0) waves = 0;
    float offset = (GameManager.level - 1) * Mathf.Sin(segment * Mathf.Deg2Rad * waves);
    offset *= 1f - Mathf.Abs(segment - 180f) / 180f;
    offset *= width / 3;
    return new Vector2(offset - width, offset + width);
  }

  public float AngleOf(Vector3 pos) {
    return Mathf.Atan2(pos.y - activeRadius, pos.z) * Mathf.Rad2Deg;
  }

  public int SegmentAt(Vector3 pos) {
    float angle = AngleOf(pos);
    return ((int)Mathf.Floor(angle - 270) + 720) % 360;
  }

  public static Vector3 tangentVector(float angle) {
    return new Vector3(0, Mathf.Cos(angle), -Mathf.Sin(angle));
  }

  public static Vector3 tangentVector(Vector3 pos) {
    return tangentVector(Mathf.Atan2(pos.y - activeRadius, pos.z));
  }

  public static Vector3 normalVector(float angle) {
    return new Vector3(0, -Mathf.Sin(angle), -Mathf.Cos(angle));
  }

  public static Vector3 normalVector(Vector3 pos) {
    return normalVector(Mathf.Atan2(pos.y - activeRadius, pos.z));
  }

  public static Quaternion tangent(float angle) {
    Vector3 forward = tangentVector(angle);
    Vector3 up = new Vector3(0, forward.z, -forward.y);
    return Quaternion.LookRotation(forward, up);
  }

  public static Quaternion tangent(Vector3 pos) {
    return tangent(Mathf.Atan2(pos.y - activeRadius, pos.z));
  }

  public void LateUpdate() {
    cars.Sort(delegate(CarBehaviour lhs, CarBehaviour rhs) {
      return (int)Mathf.Sign(rhs.distance - lhs.distance);
    });
    for (int i = 0; i < cars.Count; i++) {
      cars[i].place = i + 1;
    }
  }
}
