using UnityEngine;

public class ShipBlockPrefabs {
	public Mesh cube = null;
	public Mesh hWedge = null;
	public Mesh vWedge = null;
	public Mesh point = null;
	public Mesh topSlope = null;
	public Mesh bottomSlope = null;
  public Mesh cylinder = null;
  public Mesh wheel = null;

	public ShipBlockPrefabs() {
    cube = new Mesh();
		cube.vertices = new Vector3[] {
			new Vector3(-0.5f, -0.5f, -0.5f),
			new Vector3(+0.5f, -0.5f, -0.5f),
			new Vector3(-0.5f, +0.5f, -0.5f),
			new Vector3(+0.5f, +0.5f, -0.5f),
			new Vector3(-0.5f, -0.5f, +0.5f),
			new Vector3(+0.5f, -0.5f, +0.5f),
			new Vector3(-0.5f, +0.5f, +0.5f),
			new Vector3(+0.5f, +0.5f, +0.5f)
		};
		cube.uv = new Vector2[] {
			new Vector2(0, 0),
			new Vector2(1, 0),
			new Vector2(0, 1),
			new Vector2(1, 1),
			new Vector2(0, 0),
			new Vector2(1, 0),
			new Vector2(0, 1),
			new Vector2(1, 1)
		};
		cube.triangles = new int[] {
			0, 3, 1, 0, 2, 3, // front
			0, 1, 5, 0, 5, 4, // left
			0, 6, 2, 0, 4, 6, // top
			7, 6, 4, 7, 4, 5, // back
			7, 2, 6, 7, 3, 2, // right
			7, 5, 1, 7, 1, 3  // bottom
		};

    hWedge = new Mesh();
		hWedge.vertices = new Vector3[] {
			new Vector3(-0.5f, -0.5f, -0.5f),
			new Vector3(+0.5f, -0.5f, -0.5f),
			new Vector3(-0.5f, +0.5f, -0.5f),
			new Vector3(+0.5f, +0.5f, -0.5f),
			new Vector3(-0.5f, -0.1f, +0.5f),
			new Vector3(+0.5f, -0.1f, +0.5f),
			new Vector3(-0.5f, +0.1f, +0.5f),
			new Vector3(+0.5f, +0.1f, +0.5f)
		};
		hWedge.uv = cube.uv;
		hWedge.triangles = cube.triangles;

    vWedge = new Mesh();
		vWedge.vertices = new Vector3[] {
			new Vector3(-0.5f, -0.5f, -0.5f),
			new Vector3(+0.5f, -0.5f, -0.5f),
			new Vector3(-0.5f, +0.5f, -0.5f),
			new Vector3(+0.5f, +0.5f, -0.5f),
			new Vector3(-0.1f, -0.5f, +0.5f),
			new Vector3(+0.1f, -0.5f, +0.5f),
			new Vector3(-0.1f, +0.5f, +0.5f),
			new Vector3(+0.1f, +0.5f, +0.5f)
		};
		vWedge.uv = cube.uv;
		vWedge.triangles = cube.triangles;

    point = new Mesh();
		point.vertices = new Vector3[] {
			new Vector3(-0.5f, -0.5f, -0.5f),
			new Vector3(+0.5f, -0.5f, -0.5f),
			new Vector3(-0.5f, +0.5f, -0.5f),
			new Vector3(+0.5f, +0.5f, -0.5f),
			new Vector3(-0.1f, -0.1f, +0.5f),
			new Vector3(+0.1f, -0.1f, +0.5f),
			new Vector3(-0.1f, +0.1f, +0.5f),
			new Vector3(+0.1f, +0.1f, +0.5f)
		};
		point.uv = cube.uv;
		point.triangles = cube.triangles;

    topSlope = new Mesh();
		topSlope.vertices = cube.vertices;
		topSlope.uv = cube.uv;
		topSlope.triangles = new int[] {
			0, 7, 1, 0, 6, 7, // front
			0, 4, 6, // left
			7, 5, 1, // right
			7, 6, 4, 7, 4, 5, // back
			0, 1, 5, 0, 5, 4, // bottom
		};

    bottomSlope = new Mesh();
		bottomSlope.vertices = cube.vertices;
		bottomSlope.uv = cube.uv;
		bottomSlope.triangles = new int[] {
			2, 3, 5, 2, 5, 4, // front
			2, 4, 6, // left
			7, 5, 3, // right
			7, 6, 4, 7, 4, 5, // back
			2, 6, 7, 2, 7, 3, // top
		};

    MeshBuilder cylinderBuild = new MeshBuilder();
    Vector3 lastLeft = new Vector3(+.5f, -.5f, 0);
    Vector3 lastRight = new Vector3(+.5f, +.5f, 0);
    Vector3 hubLeft = new Vector3(0, -.5f, 0);
    Vector3 hubRight = new Vector3(0, +.5f, 0);
    float x, y;
    for (int angle = 1; angle < 33; angle++) {
      if (angle == 32) {
        x = .5f;
        y = 0;
      } else {
        x = Mathf.Cos(angle * Mathf.PI / 16f) * .5f;
        y = Mathf.Sin(angle * Mathf.PI / 16f) * .5f;
      }
      Vector3 left = new Vector3(x, -.5f, y);
      Vector3 right = new Vector3(x, +.5f, y);
      cylinderBuild.Quad("", left, right, lastLeft, lastRight, false, false);
      cylinderBuild.Triangle(left, hubLeft, lastLeft, Vector2.zero, Vector2.up, Vector2.right);
      cylinderBuild.Triangle(right, lastRight, hubRight, Vector2.zero, Vector2.up, Vector2.right);
      lastLeft = left;
      lastRight = right;
    }
    cylinder = cylinderBuild.GetMesh();

    MeshBuilder wheelBuild = new MeshBuilder();
    lastLeft = new Vector3(-.5f, +.5f, 0);
    Vector3 lastLeftI = new Vector3(-.5f, +.25f, 0);
    Vector3 lastLeftH = new Vector3(-.4f, +.25f, 0);
    lastRight = new Vector3(+.5f, +.5f, 0);
    Vector3 lastRightI = new Vector3(+.5f, +.25f, 0);
    Vector3 lastRightH = new Vector3(+.4f, +.25f, 0);
    hubLeft = new Vector3(-.4f, 0, 0);
    hubRight = new Vector3(+.4f, 0, 0);
    Vector2[][] tx = new Vector2[][] {
      new Vector2[] {
        Vector2.zero,
        new Vector2(.01f, 0),
        new Vector2(0, .01f),
        new Vector2(.01f, .01f),
      },
      new Vector2[] {
        new Vector2(0, .01f),
        Vector2.zero,
        new Vector2(.01f, .01f),
        new Vector2(.01f, 0),
      },
    };
    Vector2[][] txi = new Vector2[][] {
      new Vector2[] {
        new Vector2(.95f, 1),
        new Vector2(1, .95f),
        Vector2.one,
      },
      new Vector2[] {
        new Vector2(1, .95f),
        new Vector2(.95f, 1),
        Vector2.one,
      },
    };
    for (int angle = 1; angle < 17; angle++) {
      if (angle == 16) {
        x = .5f;
        y = 0;
      } else {
        x = Mathf.Cos(angle * Mathf.PI / 8f) * .5f;
        y = Mathf.Sin(angle * Mathf.PI / 8f) * .5f;
      }
      Vector3 left = new Vector3(-.5f, x, y);
      Vector3 right = new Vector3(+.5f, x, y);
      Vector3 leftI = new Vector3(-.5f, x * .5f, y * .5f);
      Vector3 rightI = new Vector3(+.5f, x * .5f, y * .5f);
      Vector3 leftH = new Vector3(-.4f, x * .5f, y * .5f);
      Vector3 rightH = new Vector3(+.4f, x * .5f, y * .5f);
      var otx = tx[1 - angle % 2];
      var itx = txi[angle % 2];
      // outer surface
      wheelBuild.Triangle(left, right, lastLeft, otx[0], otx[1], otx[2]);
      wheelBuild.Triangle(right, lastRight, lastLeft, otx[1], otx[3], otx[2]);
      // left wall
      wheelBuild.Triangle(left, lastLeft, leftI, otx[0], otx[1], otx[2]);
      wheelBuild.Triangle(leftI, lastLeft, lastLeftI, otx[2], otx[1], otx[3]);
      // right wall
      wheelBuild.Triangle(rightI, lastRight, right, otx[2], otx[3], otx[1]);
      wheelBuild.Triangle(rightI, lastRightI, lastRight, otx[2], otx[0], otx[3]);
      // left inset
      wheelBuild.Triangle(leftI, lastLeftI, leftH, otx[2], otx[3], itx[0]);
      wheelBuild.Triangle(leftH, lastLeftI, lastLeftH, itx[0], otx[1], itx[1]);
      // right inset
      wheelBuild.Triangle(rightH, lastRightH, rightI, itx[1], itx[0], otx[2]);
      wheelBuild.Triangle(rightI, lastRightH, lastRightI, otx[2], itx[0], otx[0]);
      // hubcaps
      wheelBuild.Triangle(leftH, lastLeftH, hubLeft, itx[0], itx[1], itx[2]);
      wheelBuild.Triangle(rightH, hubRight, lastRightH, itx[1], itx[2], itx[0]);

      lastLeft = left;
      lastRight = right;
      lastLeftI = leftI;
      lastRightI = rightI;
      lastLeftH = leftH;
      lastRightH = rightH;
    }
    wheel = wheelBuild.GetMesh();
	}
}

[System.Serializable]
public class ShipBlock {
	private static ShipBlockPrefabs _prefabs = null;

  public static ShipBlockPrefabs prefabs {
    get {
      if (_prefabs == null) {
        _prefabs = new ShipBlockPrefabs();
      }
      return _prefabs;
    }
  }

	public enum Facing {
		Forward,
		Backward,
		Left,
		Right,
		Up,
		Down
	};

	public enum GenericType {
		None,
		Cube,
		HWedge,
		VWedge,
		Point,
		TopSlope,
		BottomSlope,
    Cylinder,
    Wheel,
	};

	public Vector3 position;
	private Mesh mesh;
	public ShipBlock.Facing facing = ShipBlock.Facing.Forward;
	public GenericType genericType = GenericType.None;

	public ShipBlock(GenericType blockType, Facing facing = Facing.Forward) {
		this.facing = facing;
		this.genericType = blockType;
		switch (blockType) {
			case GenericType.Cube:
				this.mesh = prefabs.cube;
				break;
			case GenericType.HWedge:
				this.mesh = prefabs.hWedge;
				break;
			case GenericType.VWedge:
				this.mesh = prefabs.vWedge;
				break;
			case GenericType.Point:
				this.mesh = prefabs.point;
				break;
			case GenericType.TopSlope:
				this.mesh = prefabs.topSlope;
				break;
			case GenericType.BottomSlope:
				this.mesh = prefabs.bottomSlope;
				break;
       case GenericType.Cylinder:
        this.mesh = prefabs.cylinder;
        break;
       case GenericType.Wheel:
        this.mesh = prefabs.wheel;
        break;
		}
		/*
		if (uvOffset != null) {
			for (int i = 0; i < uv.Length; i++) {
				uv[i] += uvOffset;
			}
		}
		*/
	}

	public CombineInstance GetCombineInstance() {
		CombineInstance ci = new CombineInstance();
		if (this.genericType != GenericType.None) {
			this.mesh = new ShipBlock(this.genericType).mesh;
		}
		ci.mesh = this.mesh;
		ci.transform = Matrix4x4.Translate(this.position);
		switch (this.facing) {
			case ShipBlock.Facing.Backward:
				ci.transform = ci.transform * Matrix4x4.Rotate(Quaternion.Euler(0, 180, 0));
				break;
			case ShipBlock.Facing.Left:
				ci.transform = ci.transform * Matrix4x4.Rotate(Quaternion.Euler(0, -90, 0));
				break;
			case ShipBlock.Facing.Right:
				ci.transform = ci.transform * Matrix4x4.Rotate(Quaternion.Euler(0, 90, 0));
				break;
			case ShipBlock.Facing.Up:
				ci.transform = ci.transform * Matrix4x4.Rotate(Quaternion.Euler(90, 0, 0));
				break;
			case ShipBlock.Facing.Down:
				ci.transform = ci.transform * Matrix4x4.Rotate(Quaternion.Euler(-90, 0, 0));
				break;
		}
		return ci;
	}
}
