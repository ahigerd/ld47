using UnityEngine;
using System;
using System.Collections.Generic;

class MeshBuilder {
  public List<Vector3> vertices = new List<Vector3>();
  public List<Vector2> uv = new List<Vector2>();
  public List<int> triangles = new List<int>();
  public Dictionary<string, int> cache = new Dictionary<string, int>();

  public Mesh GetMesh(bool addWireframe = false) {
    Mesh mesh = new Mesh();
    mesh.vertices = vertices.ToArray();
    mesh.uv = uv.ToArray();
    mesh.triangles = triangles.ToArray();
    if (addWireframe) {
      Mesh wire = ConvertToWireFrame(mesh);
      mesh.subMeshCount = 2;
      mesh.SetIndices(wire.GetIndices(0), MeshTopology.Lines, 1);
    }
    return mesh;
  }

  public Mesh GetWireFrame() {
    Mesh mesh = new Mesh();
    mesh.vertices = vertices.ToArray();
    mesh.uv = uv.ToArray();
    int[] lines = new int[triangles.Count * 2];
    int j = -1;
    for (int i = 0; i < triangles.Count; i += 3) {
      lines[++j] = triangles[i + 0];
      lines[++j] = triangles[i + 1];
      lines[++j] = triangles[i + 1];
      lines[++j] = triangles[i + 2];
      lines[++j] = triangles[i + 2];
      lines[++j] = triangles[i + 0];
    }
    mesh.SetIndices(lines, MeshTopology.Lines, 0);
    return mesh;
  }

  public int Get(float x, float y, float z, float u, float v) {
    return Get("", x, y, z, u, v);
  }

  public int Get(string tag, float x, float y, float z, float u, float v) {
    Vector3 vec = new Vector3(x, y, z);
    string key = tag + vec.ToString();
    if (cache.ContainsKey(key)) {
      return cache[key];
    }
    int index = vertices.Count;
    cache.Add(key, index);
    vertices.Add(vec);
    uv.Add(new Vector2(u, v));
    return index;
  }

  public int Get(Vector3 point, Vector2 uv) {
    return Get("", point.x, point.y, point.z, uv.x, uv.y);
  }

  public int Get(string key, Vector3 point, Vector2 uv) {
    return Get(key, point.x, point.y, point.z, uv.x, uv.y);
  }

  public void Triangle(Vector3 a, Vector3 b, Vector3 c, Vector2 uva, Vector2 uvb, Vector2 uvc) {
    this.Triangle("", a, b, c, uva, uvb, uvc);
  }

  public void Triangle(string key, Vector3 a, Vector3 b, Vector3 c, Vector2 uva, Vector2 uvb, Vector2 uvc) {
    int v1 = Get(key, a, uva);
    int v2 = Get(key, b, uvb);
    int v3 = Get(key, c, uvc);
    triangles.Add(v1);
    triangles.Add(v2);
    triangles.Add(v3);
  }

  public void XZQuad(float xMin, float zMin, float xMax, float zMax, float y, bool flipU, bool flipV) {
    int v1 = Get("xz", xMin, y, zMin, flipU ? 1 : 0, flipV ? 1 : 0);
    int v2 = Get("xz", xMax, y, zMin, flipU ? 0 : 1, flipV ? 1 : 0);
    int v3 = Get("xz", xMin, y, zMax, flipU ? 1 : 0, flipV ? 0 : 1);
    int v4 = Get("xz", xMax, y, zMax, flipU ? 0 : 1, flipV ? 0 : 1);
    triangles.Add(v1);
    triangles.Add(v3);
    triangles.Add(v2);
    triangles.Add(v3);
    triangles.Add(v4);
    triangles.Add(v2);
  }

  public void XYQuad(float xMin, float yMin, float xMax, float yMax, float z, bool flipU, bool flipV) {
    int v1 = Get("xy", xMin, yMin, z, flipU ? 1 : 0, flipV ? 1 : 0);
    int v2 = Get("xy", xMax, yMin, z, flipU ? 0 : 1, flipV ? 1 : 0);
    int v3 = Get("xy", xMin, yMax, z, flipU ? 1 : 0, flipV ? 0 : 1);
    int v4 = Get("xy", xMax, yMax, z, flipU ? 0 : 1, flipV ? 0 : 1);
    triangles.Add(v1);
    triangles.Add(v3);
    triangles.Add(v2);
    triangles.Add(v3);
    triangles.Add(v4);
    triangles.Add(v2);
  }

  public void YZQuad(float yMin, float zMin, float yMax, float zMax, float x, bool flipU, bool flipV) {
    int v1 = Get("yz", x, yMin, zMin, flipU ? 1 : 0, flipV ? 1 : 0);
    int v2 = Get("yz", x, yMax, zMin, flipU ? 0 : 1, flipV ? 1 : 0);
    int v3 = Get("yz", x, yMin, zMax, flipU ? 1 : 0, flipV ? 0 : 1);
    int v4 = Get("yz", x, yMax, zMax, flipU ? 0 : 1, flipV ? 0 : 1);
    triangles.Add(v1);
    triangles.Add(v3);
    triangles.Add(v2);
    triangles.Add(v3);
    triangles.Add(v4);
    triangles.Add(v2);
  }

  public void Quad(string key, Vector3 a, Vector3 b, Vector3 c, Vector3 d, bool flipU, bool flipV) {
    int v1 = Get(key, a.x, a.y, a.z, flipU ? 1 : 0, flipV ? 1 : 0);
    int v2 = Get(key, b.x, b.y, b.z, flipU ? 0 : 1, flipV ? 1 : 0);
    int v3 = Get(key, c.x, c.y, c.z, flipU ? 1 : 0, flipV ? 0 : 1);
    int v4 = Get(key, d.x, d.y, d.z, flipU ? 0 : 1, flipV ? 0 : 1);
    triangles.Add(v1);
    triangles.Add(v3);
    triangles.Add(v2);
    triangles.Add(v3);
    triangles.Add(v4);
    triangles.Add(v2);
  }

  public static Mesh ConvertToWireFrame(Mesh source) {
    Mesh result = new Mesh();
    result.vertices = source.vertices;
    result.uv = source.uv;
    List<int> lines = new List<int>();
    var edges = new HashSet<Tuple<int, int>>();
    for (int i = 0; i < source.triangles.Length; i += 3) {
      int lastVertex = source.triangles[i + 2];
      for (int j = 0; j < 3; j++) {
        int vertex = source.triangles[i + j];
        var edge = Tuple.Create(vertex, lastVertex);
        var flipEdge = Tuple.Create(lastVertex, vertex);
        if (!edges.Contains(edge) && !edges.Contains(flipEdge)) {
          edges.Add(edge);
          lines.Add(vertex);
          lines.Add(lastVertex);
        }
        lastVertex = vertex;
      }
    }
    result.SetIndices(lines.ToArray(), MeshTopology.Lines, 0);
    return result;
  }
}
