﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class ColorStop
{
	[Range(-1.0f, 1.0f)]
		public float curve = 0.0f;
	[Range(0.0f, 1.0f)]
		public float stop;
	public Color color;
}

public class SkyboxGradient : MonoBehaviour
{
	public int SIZE = 128;
	public ColorStop[] colors;
	public bool setFog = true;
	public bool updateFog = false;

	private float CENTER;

	private Texture2D top;
	private Texture2D bottom;
	private Texture2D side;
	private Color currentFogColor;
	private Color horizon;

	void Start()
	{
		CENTER = (SIZE - 1) / 2.0f;

		for (var i=0; i < colors.Length; i++)
			colors[i].color.a = 1;

		top = new Texture2D(SIZE, SIZE);
		bottom = new Texture2D(SIZE, SIZE);
		side = new Texture2D(SIZE, SIZE);

		side.wrapMode = TextureWrapMode.Clamp;
		top.wrapMode = TextureWrapMode.Clamp;
		bottom.wrapMode = TextureWrapMode.Clamp;

		if (RenderSettings.skybox==null) 
			RenderSettings.skybox = new Material(Shader.Find ("RenderFX/Skybox"));

		Material skyMaterial = RenderSettings.skybox;

		skyMaterial.SetTexture("_UpTex", top);
		skyMaterial.SetTexture("_DownTex", bottom);
		skyMaterial.SetTexture("_FrontTex", side);
		skyMaterial.SetTexture("_BackTex", side);
		skyMaterial.SetTexture("_LeftTex", side);
		skyMaterial.SetTexture("_RightTex", side);

		UpdateSkybox();

		currentFogColor = horizon;
	}

	void Update()
	{
		if (setFog && updateFog) RenderSettings.fogColor = horizon;
	}

	private float WeightedCurve(float val, float curve)
	{
		float adj = Mathf.Exp(-curve);
		return Mathf.Pow(1.0f - Mathf.Pow(1.0f - val, adj), 1.0f / adj);
	}

	private Color MultiColorLerp(float val)
	{
		if(val < colors[0].stop) return colors[0].color;
		for(var i = 1; i < colors.Length; i++) {
			if(colors[i].stop >= val) {
				val = (val - colors[i-1].stop) / (colors[i].stop - colors[i-1].stop);
				return Color.Lerp(colors[i-1].color, colors[i].color, WeightedCurve(val, colors[i].curve));
			}
		}
		return colors[colors.Length - 1].color;
	}
	
	public void UpdateSkybox()
	{
		float x;
		float y = CENTER + 0.25f;
		float z;
		float x_squared;
		float y_squared = y * y;
		float mag;
		float phi;
		int i;
		int j;
		int flip_i;
		Color c;

		horizon = MultiColorLerp(0.5f);

		for(i = 0; i < CENTER; i++) {
			x = i - CENTER;
			x_squared = x * x;
			flip_i = SIZE - i - 1;
			for(j = 0; j < SIZE; j++) {
				z = j - CENTER;
				mag = Mathf.Sqrt(x_squared + y_squared + (z * z));
				phi = Mathf.Acos(z / mag);
				c = MultiColorLerp(phi / Mathf.PI);
				side.SetPixel(i, j, c);
				side.SetPixel(flip_i, j, c);

				phi = Mathf.Acos(y / mag); // Coordinate substitution: swap y and z
				c = MultiColorLerp(phi / Mathf.PI);
				top.SetPixel(i, j, c);
				top.SetPixel(flip_i, j, c);

				c = MultiColorLerp(1.0f - (phi / Mathf.PI));
				bottom.SetPixel(i, j, c);
				bottom.SetPixel(flip_i, j, c);
			}
		}
		side.Apply();
		top.Apply();
		bottom.Apply();

		if (setFog) 
			RenderSettings.fogColor = horizon;
	}
}

